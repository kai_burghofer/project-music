package com.qualitype.education.music;

import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@RequestScoped
@Transactional
@Path("/albums")
public class AlbumsResource {

	@PersistenceContext
	private EntityManager em;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllAlbums(@QueryParam("name") final String nameParam,
			@QueryParam("artist") final String artistParam, @QueryParam("release") final String releaseParam) {
		try {
			final var query = createSelectQuery(nameParam, artistParam, validateIntegerParam(releaseParam));
			final var result = query.getResultList();
			final var albums = result.stream().map(this::toAlbum).collect(Collectors.toList());
			return Response.ok(albums).build();
		} catch (final NumberFormatException ex) {
			return Response.status(Status.BAD_REQUEST).build();
		}
	}

	private static Integer validateIntegerParam(final String param) throws NumberFormatException {
		if (param != null)
			return Integer.valueOf(param);
		return null;
	}

	private TypedQuery<AlbumEntity> createSelectQuery(final String nameFilter, final String artistFilter,
			final Integer releaseFilter) {
		final var jpaql = new StringBuilder("SELECT a FROM Album a ");

		var filterCount = 0;

		if (nameFilter != null)
			appendFilter(jpaql, filterCount++, "name");
		if (artistFilter != null)
			appendFilter(jpaql, filterCount++, "artist");
		if (releaseFilter != null)
			appendFilter(jpaql, filterCount++, "release");

		jpaql.append("ORDER BY a.name, a.id");

		final var query = this.em.createQuery(jpaql.toString(), AlbumEntity.class);
		if (nameFilter != null)
			query.setParameter("name", nameFilter);
		if (artistFilter != null)
			query.setParameter("artist", artistFilter);
		if (releaseFilter != null)
			query.setParameter("release", releaseFilter);
		return query;
	}

	private static void appendFilter(final StringBuilder jpaql, final int filterCount, final String property) {
		jpaql.append(filterCount == 0 ? "WHERE" : "AND");
		jpaql.append(" a.").append(property).append(" = :").append(property).append(" ");
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAlbum(@PathParam("id") final long id) {
		final var entity = this.em.find(AlbumEntity.class, id);

		if (entity != null) {
			final var value = toAlbum(entity);
			return Response.ok().entity(value).build();
		}

		return Response.status(Status.NOT_FOUND).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createAlbum(final Album album) {
		final AlbumEntity entity = new AlbumEntity();
		fromAlbum(entity, album);

		em.persist(entity);

		return Response.status(Status.CREATED).build();
	}

	@DELETE
	@Path("{id}")
	public Response deleteAlbum(@PathParam("id") final long id) {
		final var entity = this.em.find(AlbumEntity.class, id);

		if (entity != null) {
			this.em.remove(entity);
			return Response.ok().build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateAlbum(@PathParam("id") final long id, final Album album) {
		final var entity = em.find(AlbumEntity.class, id);

		if (entity != null) {
			fromAlbum(entity, album);
			return Response.ok().build();
		}

		return Response.status(Status.NOT_FOUND).build();
	}

	private Album toAlbum(final AlbumEntity entity) {
		final var album = new Album();
		album.id = entity.getId();
		album.name = entity.getName();
		album.artist = entity.getArtist();
		album.release = entity.getRelease();
		album.pal = entity.isPal();
		return album;
	}

	private void fromAlbum(final AlbumEntity entity, final Album album) {
		entity.setName(album.name);
		entity.setArtist(album.artist);
		entity.setRelease(album.release);
		entity.setPal(album.pal);
	}

}
