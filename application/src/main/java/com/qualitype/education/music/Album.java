package com.qualitype.education.music;

public class Album {
	
	public long id;

	public String name;
	
	public String artist;
	
	public Integer release;
	
	public boolean pal;
}
