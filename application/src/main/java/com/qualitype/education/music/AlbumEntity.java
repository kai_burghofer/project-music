package com.qualitype.education.music;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name = "Album")
@Table(name = "albums", schema = "public")
public class AlbumEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "album_id_generator")
	@SequenceGenerator(name = "album_id_generator", sequenceName = "album_sequence", initialValue = 1, allocationSize = 1)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "artist", nullable = false)
	private String artist;

	@Column(name = "release", nullable = false)
	private Integer release;

	@Column(name = "pal", nullable = false)
	private boolean pal;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(final String artist) {
		this.artist = artist;
	}

	public Integer getRelease() {
		return release;
	}

	public void setRelease(final Integer release) {
		this.release = release;
	}

	public boolean isPal() {
		return pal;
	}

	public void setPal(final boolean pal) {
		this.pal = pal;
	}

}
