@ECHO OFF

REM shut down and remove all containers
REM remove the dynamically created volumes as well (we start fresh every time)
docker-compose down

REM --volumes