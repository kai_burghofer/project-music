CREATE TABLE albums
(
    id INTEGER NOT NULL,
    name character varying NOT NULL,
    artist character varying NOT NULL,
    release INTEGER NOT NULL,
    pal boolean NOT NULL
);
CREATE SEQUENCE album_sequence INCREMENT 1 START 1;

INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Four Monkeys', 'Millencolin', '1997', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Stranger Than Fiction', 'Bad Religion', '1994', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'How Could Hell Be Any Worse', 'Bad Religion', '1983', true);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Suffer', 'Bad Religion', '1988', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'No Control', 'Bad Religion', '1989', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Tiny Tunes', 'Millencolin', '1994', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Life On A Plate', 'Millencolin', '1995', true);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Unknown Road', 'Pennywise', '1993', true);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'All Or Nothing', 'Pennywise', '2012', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Never Gonna Die', 'Pennywise', '2018', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'All Or Nothing', 'The Subways', '2008', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Money And Celebrity', 'The Subways', '2011', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Young For Eternity', 'The Subways', '2005', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'No Control', 'Eddie Money', '1983', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Atom Heart Mother', 'Pink Floyd', '1970', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'The Dark Side Of The Moon', 'Pink Floyd', '1973', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Animals', 'Pink Floyd', '1977', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Strange Days', 'The Doors', '1967', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Waiting For The Sun', 'The Doors', '1968', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Full Circle', 'The Doors', '1972', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Pendulum', 'Creedence Clearwater Revival', '1970', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Mardi Gras', 'Creedence Clearwater Revival', '1972', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Green River', 'Creedence Clearwater Revial', '1969', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'A Broke Frame', 'Depeche Mode', '1982', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Some Great Reward', 'Depeche Mode', '1984', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Songs Of Faith an Devotion', 'Depeche Mode', '1993', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Check your Head', 'Beastie Boys', '1992', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Hello Nasty', 'Beastie Boys', '1998', false);
INSERT INTO public.albums(id, name, artist, release, pal) VALUES (nextval('album_sequence'), 'Ill Communication', 'Beastie Boys', '1994', false);
